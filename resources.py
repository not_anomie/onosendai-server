# Should automatically create a dictionary of each texture path
# and it's ID. This makes it much easier to send to the client.

#texture_dict = {
#    0:"resources/default.png",
#    1:"resources/cassette.png",
#    2:"resources/token.png",
#    3:"resources/conf.gif"
#    }

texture_dict = {
    0:"resources/Player/Abigail/Abigail.png",
    1:"resources/Player/Abigail/Abigail_wlk_l.gif",
    2:"resources/Player/Abigail/Abigail_wlk_r.gif",
    3:"resources/Player/Abigail/Abigail_wlk_b.gif",
    4:"resources/Player/Abigail/Abigail_wlk_f.gif",
    5:"resources/Player/Abigail/Abigail_laugh.gif",
    6:"resources/Player/Abigail/Abigail_sit.gif",
    7:"resources/default.png",
    8:"resources/cassette.png",
    9:"resources/token.png",
    10:"resources/conf.gif",
    11:"resources/SVHouse/Borders.png",
    12:"resources/SVHouse/Floors.png",
    13:"resources/SVHouse/Room.png",
    14:"resources/SVHouse/Walls.png",
    15:"resources/Furniture/Bookshelf_0.png",
    16:"resources/Furniture/TV_Small.png"
    }
