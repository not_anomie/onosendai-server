import pyglet
from copy import deepcopy
import resources
from pyglet.gl import *
import node_types as nt

## SETTINGS VARIABLES
general_scale = 3.0
panning_friction = 0.5

## NODE FORMATS
decorative_node_form = "world.add_node(nt.decorative_node({x},{y},z={z}, texture_id={txtr}))"
hitbox_node_form = "world.add_node(nt.hitbox_node({x},{y},{w},{l}))"

## RUNTIME VARIABLES
z_order_mode = 0
selected_texture = 0
mouse_location = 0, 0
pan_offset = [0,0]
world_data = []
world_data_as_sprites = []
panning = False
zoom_level = 1.00
selected_sprite = None # Redefined after texture loading
crosshair_label = pyglet.text.Label(text="+", font_size=20, x=0, y=0, anchor_x="center", anchor_y="center")
placement_data_label = pyglet.text.Label(text='', font_size=15, x=0, y=0, width=200, multiline=True)
preview_sprite = None # Redefined after texture loading
selector_sprite = None # Redefined after texture loading
preview_mode = False
making_hitbox = False
hit_start = [0,0]
node_mode = "DECOR"

# Create the game window
window = pyglet.window.Window(1100,800)

# Set the resources folder
pyglet.resource.path = ['.']
pyglet.resource.reindex()

# Centers and scales an image (Pass by refrence)
def process_image(img) :
    img.width = img.width * general_scale
    img.height = img.height * general_scale
    img.anchor_x = img.width / 2.0
    img.anchor_y = img.height / 2.0

# Loads and processes an image based off of a directory
def load_image(path) :
    img = pyglet.resource.image(path)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
    process_image(img)
    return img

# Load in an editor resource
wb_texture_bin = {}
def load_editor_image (name, path) :
    wb_texture_bin[name] = load_image("wb_assets/"+path)

# Load in the games resources
pyglet_texture_bin = deepcopy(resources.texture_dict)
for i in pyglet_texture_bin :
    pyglet_texture_bin[i] = load_image(pyglet_texture_bin[i])

# Make (0,0) the center of the screen
glTranslatef(window.width/2.0, window.height/2.0, 0)

# Declare the editor resources
load_editor_image ("crosshair", "Crosshair.png")
load_editor_image ("hit_top", "hitbox_topleft.png")
wb_texture_bin["hit_top"].anchor_x = 0
wb_texture_bin["hit_top"].anchor_y =  wb_texture_bin["hit_top"].width
load_editor_image ("hit_bot", "hitbox_botright.png")
wb_texture_bin["hit_bot"].anchor_x = wb_texture_bin["hit_bot"].width
wb_texture_bin["hit_bot"].anchor_y = 0
load_editor_image ("menubar", "MenuBar.png")
load_editor_image ("selector", "MenuBarSelector.png")

# Re-declare the texure previewing sprite (aka selected_sprite)
selected_sprite = pyglet.sprite.Sprite(
    img=pyglet_texture_bin[selected_texture],
    x=pyglet_texture_bin[selected_texture].width/2.0 - window.width/2.0,
    y=pyglet_texture_bin[selected_texture].height/2.0 - window.height/2.0
    )

# Re-declare the previewing sprite
preview_sprite = pyglet.sprite.Sprite(
    img=pyglet_texture_bin[selected_texture],
    x=0,
    y=0
    )
preview_sprite.opacity = 255.0/2

# Re-declare the selecting sprite ( The triangle thingy on the menubar )
selector_sprite = pyglet.sprite.Sprite(
    img=wb_texture_bin["selector"],
    x=0,
    y=window.height/2.0 - wb_texture_bin["menubar"].height
    )
selector_sprite.rotation = 180

# Move the placement_data_label to match the window's size
placement_data_label.y = window.height/2.0 - 20
placement_data_label.x = -window.width/2.0 + 4

# Writes world_data out to world_data.py
def write_world_data() :
    opn = open("world_data.py", 'w')
    for i in world_data : opn.write(i + "\n")
    opn.close()
    print("World data saved!")

# Erases the current holdings for world_data and world_data_as_sprites
def reset_world_data() :
    global world_data
    world_data = []
    add_world_data_prereqs()
    global world_data_as_sprites
    world_data_as_sprites = []

# Make the pre-reqs for the world data
def add_world_data_prereqs() :
    world_data.append("import node_types as nt")
    world_data.append("import world as wrld")
    world_data.append("world = wrld.server_world()")
    world_data.append("def get_world() : return world")

# Loads in the world data from the existing world_data.py
world_data = []
try : 
    opn = open("world_data.py", 'r')
    world_data = [ i.strip("\n") for i in opn.readlines() ]
except :
    reset_world_data()
    write_world_data()

# Puts that existing world_data into the sprite visualization
import world_data as wd_file
try :
    w = wd_file.get_world()
except :
    print("ERROR: The world_data.py file seems corrupt...")
for i in w.nodes :
    if isinstance(i, nt.decorative_node) :
        world_data_as_sprites.append(pyglet.sprite.Sprite(
            img = pyglet_texture_bin[i.texture_id],
            x = i.x,
            y = i.y))
        world_data_as_sprites[-1].z_order = i.z/-1000
    if isinstance(i, nt.hitbox_node) :
       world_data_as_sprites.append(pyglet.sprite.Sprite(
            img = wb_texture_bin["hit_top"],
            x = i.x-i.w/2.0,
            y = i.y+i.l/2.0))
       world_data_as_sprites[-1].z_order = 200
       world_data_as_sprites.append(pyglet.sprite.Sprite(
            img = wb_texture_bin["hit_bot"],
            x = i.x + i.w/2.0,
            y = i.y - i.l/2.0,))
       world_data_as_sprites[-1].z_order = 200

@window.event
def on_draw() :
    # Clears the wi- look you know what it does.
    window.clear()

    # Apply the pan
    glTranslatef(pan_offset[0], pan_offset[1], 0)
    
    # Draw all the sprites in the "world_data_as_sprites" array
    for i in world_data_as_sprites : i.draw()

    # Unapply the pan
    glTranslatef(-pan_offset[0], -pan_offset[1], 0)

    # Draw the previewing sprite (The non-opaque thing by the cursor)
    if preview_mode : preview_sprite.draw()

    # Unapply the zoom
    glScaled(1/zoom_level, 1/zoom_level, 0)

    # Draw the currently selected texture at the bottom left of the screen
    selected_sprite.draw()
    
    # Draw a crosshair (as text)
    crosshair_label.draw()

    # Draw the menubar
    wb_texture_bin["menubar"].blit(0, window.height/2.0-wb_texture_bin['menubar'].height/2.0)

    # Draw the selector ( Triangle thingy by the menubar )
    selector_sprite.draw()

    # Draw the placement data label
    placement_data_label.draw()

    # Re-apply the zoom
    glScaled(zoom_level, zoom_level, 0)

@window.event
def on_key_press(key, mod) :

    if key == pyglet.window.key.LEFT :
        if selected_texture > 0 :
            global selected_texture
            selected_texture -= 1
            update_selected_sprite()

    if key == pyglet.window.key.RIGHT :
        if selected_texture < len(pyglet_texture_bin)-1 :
            global selected_texture
            selected_texture += 1
            update_selected_sprite()

    if key == pyglet.window.key.UP :
        global z_order_mode
        z_order_mode += 1
        update_placement_data_label()

    if key == pyglet.window.key.DOWN :
        global z_order_mode
        z_order_mode -= 1
        update_placement_data_label()

    if key == pyglet.window.key.EQUAL :
        global zoom_level
        zoom_level = zoom_level * 1.100
        glScaled(1.100, 1.100, 0)

    if key == pyglet.window.key.MINUS :
        global zoom_level
        zoom_level = zoom_level / 1.100
        glScaled(1/1.100, 1/1.100, 0)

    if key == pyglet.window.key.LCTRL :
        global preview_mode
        preview_mode = not preview_mode

    if key == pyglet.window.key._1 :
        global node_mode
        node_mode = "DECOR"
        update_selected_sprite()
        update_placement_data_label()

    if key == pyglet.window.key._2 :
        global node_mode
        node_mode = "HITBOX"
        update_selected_sprite()
        update_placement_data_label()

    if key == pyglet.window.key.RETURN :
        if node_mode == "DECOR" : add_new_decor_node()
        if node_mode == "HITBOX" : make_hitbox_key_call()

    if key == pyglet.window.key.BACKSPACE :
        delete_node()

    if mod == pyglet.window.key.MOD_CTRL and key == pyglet.window.key.S :
        write_world_data()

    if mod == pyglet.window.key.MOD_CTRL and key == pyglet.window.key.N :
        reset_world_data()

@window.event
def on_mouse_drag(x, y, dx, dy, key, mod) :
    update_mouse_position(x,y)
    if key == pyglet.window.mouse.RIGHT :
        global panning
        panning = True
        pan_offset[0] += dx
        pan_offset[1] += dy

@window.event
def on_mouse_motion(x, y, dx, dy) :
    update_mouse_position(x,y)

@window.event
def on_mouse_release(x, y, k, mod) :
    global panning
    panning = False

# Updates the mouse's location
def update_mouse_position(x, y) :
    global mouse_location
    mouse_location = x - window.width/2.0, y - window.height/2.0

# Updates the information on the currently selected node(s)
def update_selected_sprite() :
    # The sprite at the bottom left corner 
    selected_sprite.image = pyglet_texture_bin[selected_texture]
    if node_mode == "HITBOX" :
        selected_sprite.opacity = 0
        preview_sprite.opacity = 0
    if node_mode == "DECOR" :
        selected_sprite.opacity = 255
        preview_sprite.opacity = 255/2.0
    selected_sprite.x     = selected_sprite.image.width/2.0 - window.width/2.0 + 4
    selected_sprite.y     = selected_sprite.image.height/2.0 - window.height/2.0 + 4
    preview_sprite.image  = pyglet_texture_bin[selected_texture]

    # The triangle thingy by the menu bar
    if node_mode == "DECOR" : selector_sprite.x = -40 * general_scale
    if node_mode == "HITBOX" : selector_sprite.x = (-40+16) * general_scale

# Updates the label that informs the user of placement data (like z_order-ing)
def update_placement_data_label() :
    if node_mode == "DECOR" :
        placement_data_label.text = "Z_Order: {z}\nNode Type: {ntype}".format(z=z_order_mode, ntype=node_mode)
    if node_mode == "HITBOX" :
        placement_data_label.text = "Node Type: {ntype}".format(z=z_order_mode, ntype=node_mode)

# Finds the z_ordering score
def find_z_val(obj) :
    if not hasattr(obj, "z_order") : return 1000000
    return obj.z_order*-10000 + obj.y

# Re-organizes the drawing list with regards to the sprites z_order
def update_z_orders() :
    global world_data_as_sprites
    world_data_as_sprites = sorted( world_data_as_sprites, key=lambda o: find_z_val(o), reverse=True)

# Adds a new node at the current cursor position, texture, and Z-order
def add_new_decor_node() :
        s = pyglet.sprite.Sprite( x=-pan_offset[0], y=-pan_offset[1], img=pyglet_texture_bin[selected_texture] )
        s.z_order = z_order_mode
        world_data_as_sprites.append(s)
        update_z_orders()
        world_data.append(decorative_node_form.format(
            x=-pan_offset[0],
            y=-pan_offset[1],
            z=z_order_mode*-10000,
            txtr=selected_texture
            ))
        print world_data[-1]

# Start making a hitbox
def start_hitbox() :
    global making_hitbox
    making_hitbox = True
    global hit_start
    hit_start[0] = -pan_offset[0]
    hit_start[1] = -pan_offset[1]
    print("Starting hitbox at " + str(hit_start) )

# Ends the current working hitbox
def end_hitbox() :
    if -pan_offset[0] < hit_start[0] : return
    if -pan_offset[1] > hit_start[1] : return
    w = -pan_offset[0] - hit_start[0]
    l = -1*(-pan_offset[1] - hit_start[1])
    print("Ending hitbox at " + str([w,l]) )
    global making_hitbox
    making_hitbox = False
    # Adds to the world_data.py file
    world_data.append(hitbox_node_form.format(
        x=int(hit_start[0]+(w/2)),
        y=int(hit_start[1]-(l/2)),
        w=w,
        l=l))
   
   # Adds the visualization to the editors version of the world data
    world_data_as_sprites.append(pyglet.text.Label(text="I", color=[0,255,0,255], anchor_x="center", anchor_y="center",
        x=hit_start[0], y=hit_start[1]))

    world_data_as_sprites.append(pyglet.text.Label(text="O", color=[0,255,0,255], anchor_x="center", anchor_y="center",
        x=hit_start[0]+w, y=hit_start[1]-l))

    world_data_as_sprites.append(pyglet.sprite.Sprite(img=wb_texture_bin["hit_top"], x=hit_start[0], y=hit_start[1]))
    world_data_as_sprites.append(pyglet.sprite.Sprite(img=wb_texture_bin["hit_bot"], x=hit_start[0]+w, y=hit_start[1]-l))

    print(world_data[-1])

# To be called when node_mode is hitboxes, and a return is presed
def make_hitbox_key_call() :
    if making_hitbox : end_hitbox()
    else : start_hitbox()

# Deletes the top-most node under the cursor
# It's also the single dumbest thing I have ever written
# and sort of a security hazard...
def delete_node() :
    for i in world_data_as_sprites :
        if not hasattr(i, "image") : continue
        if not abs(-pan_offset[0] - i.x) < i.image.width/2.0 : continue
        if not abs(-pan_offset[1] - i.y) < i.image.height/2.0: continue
        for j in world_data[4:] :
            k = j + "\n"
            k = k.replace(")\n", '')
            k = k.replace("world.add_node(", "")
            a = eval(k) # That's the security hazard
            if (a.x, a.y) == (i.x, i.y) :
                print("Removing " + str(k) )
                world_data.remove(j)
                world_data_as_sprites.remove(i)
                return
        print("Nothing to delete")
            

# Because I'm too lazy the change the starting values of "selector_sprite"
update_selected_sprite()
update_z_orders()

pyglet.app.run()
