import wrapper as wr
class decorative_node :
    def __init__(self,x,y,z=0, texture_id=0) :
        self.node_id = wr.node_count
        wr.node_count += 1
        self.texture_id = texture_id
        self.x, self.y, self.z = x, y, z

    def as_packet(self) :
        pckt = "Decorative_Node|{txt}|{x}|{y}|{z}|{id}\r\n".format(
            txt = self.texture_id,
            x = self.x,
            y = self.y,
            z = self.z,
            id = self.node_id)
        return pckt

class description_node :
    def __init__(self,x,y,z=0, texture_id=0, desc='') :
        self.desc = desc
        self.node_id = wr.node_count
        wr.node_count += 1
        self.texture_id = texture_id
        self.x, self.y, self.z = x, y, z

    def as_packet(self) :
        pckt = "Description_Node|{txt}|{x}|{y}|{z}|{id}|{dsc}\r\n".format(
            txt = self.texture_id,
            x = self.x,
            y = self.y,
            z = self.z,
            id = self.node_id,
            dsc = self.desc)
        return pckt

class text_node :
    def __init__(self,x,y,z=0, text="") :
        self.node_id = wr.node_count
        wr.node_count += 1
        self.text = text.replace("|", '-').replace("\\", '\\\\') # I really need to make a filter function
        self.x, self.y, self.z = x, y, z

    def as_packet(self) :
        pckt = "Decorative_Node|{txt}|{x}|{y}|{z}|{id}\r\n".format(
            txt = self.text,
            x = self.x,
            y = self.y,
            z = self.z,
            id = self.node_id)
        return pckt
        

class hitbox_node :
    def __init__(self, x, y, w, l) :
        self.x, self.y, self.w, self.l = x, y, w, l
        self.node_id = wr.node_count
        wr.node_count += 1

    def as_packet(self) :
        pckt = "Hitbox_Node|{x}|{y}|{w}|{l}|{id}\r\n".format(
            x = self.x,
            y = self.y,
            w = self.w,
            l = self.l,
            id = self.node_id)
        return pckt
            

class player_node :
    def __init__(self, x=0, y=0, name="Player?", texture_id=9) :
        self.x, self.y = x, y
        self.name = name
        self.moving = False
        self.texture_id = texture_id
        self.node_id = wr.node_count
        wr.node_count += 1

    def as_packet(self) :
        pckt = "Player_Node|{txt}|{x}|{y}|{y}|{id}|{name}|{moving}\r\n".format(
           txt=self.texture_id,
           x = self.x,
           y = self.y,
           id = self.node_id,
           name = self.name,
           moving = int(self.moving))
        return pckt
