import SocketServer
import resources as r
import sys
import world
import node_types as nt
import world_data
import wrapper as w

# Set the server port
port = 5000
if len(sys.argv) >= 2 : port = int(sys.argv[1])

# Load the main world object
w.worlds = world_data.get_worlds()
main_world = w.worlds[0] # This is the default world, so it gets to be special ;)

# A list containing every client_connection object
client_list = []

# The main client connection object
class client_connection (SocketServer.BaseRequestHandler):
    # This function is called at the start of a client connection
    def setup(self) :
        print("\nNew connection!")
        global client_list
        client_list.append(self)
        print("    Making Player Node...")
        self.node = nt.player_node()
        main_world.add_node(self.node)
        print("     Done!")
        self.packet_q = []
        self.current_world = 0

    # The base handling function for the client connect (Manages the recieving and cleaning of inputs/packets)
    def handle(self) :
        data = ""    
        while True :
            d = self.request.recv(1)
            data += d
            if d == '' :
                return
            if "\r\n" in data :
                data = data.replace("\r\n", '')
                self.parse_input(data)
                data = ""

    def finish(self) :
        print("Player " + self.node.name + " left the server.")
        client_list.remove(self)
        self.delete_player_node()
        self.request.close()

    def delete_player_node(self) :
        for i in client_list :
            i.request.send("Delete_Node|"+str(self.node.node_id)+"\r\n")
        for i in range(len(main_world.nodes)) :
            if main_world.nodes[i] == self.node :
               main_world.nodes.remove(self.node) 
    
    # Depending on the packet sent by the user, do some stuff, and then send a packet back!
    def parse_input(self, data) :
        data_s = data.split("|")
        
        if data_s[0] == "Close_Connection" :
            self.finish()

        if data_s[0] == "Get_Texture_Dictionary" :
            self.send_all_texture_binaries()

        if data_s[0] == "Get_Node_Data" :
            self.send_all_node_data()

        if data_s[0] == "Get_Node_Delta_Data" :
            self.send_dynamic_node_data()

        if data_s[0] == "Set_Player_Position" and len(data_s) == 3 :
            self.set_player_position(data_s[1], data_s[2])

        if data_s[0] == "Set_Chat_Message" and len(data_s) == 2 :
            self.set_chat_msg(data_s[1])
            print(data_s)

        if data_s[0] == "Get_Chat_Messages" :
            self.send_chat_msgs()

        if data_s[0] == "Get_Packet_Q" :
            self.send_packet_q()

        if data_s[0] == "Set_Name" and len(data_s) == 2 :
            self.set_name(data_s[1])

        if data_s[0] == "Set_Player_Texture" and len(data_s) == 3 :
            self.set_texture(data_s[1], data_s[2])

        if data_s[0] == "Set_Current_World" and len(data_s) == 2 :
            self.change_world(int(data_s[1]))


    # Changes the players current world
    def change_world(self, world_num):
        self.send_packet_q()
        self.delete_player_node()
        self.current_world = world_num
        w.worlds[self.current_world].add_node(self.node)
        self.empty_world_buffer()
        self.send_all_node_data()

    # Empties out the players current world buffer
    def empty_world_buffer(self):
        self.request.send("Clear_Nodes\r\n")

    # Tells the player to clear out the node buffer
    def reset_buffer(self) :
        self.request.send("Clear_Node_Buffer\r\n")

    # Sets the players current texture_id
    def set_texture(self, id, moving) :
        if int(moving) == 0 : self.node.moving = False
        if int(moving) == 1 : self.node.moving = True
        self.node.texture_id = int(id)

    # Sets the players username
    def set_name(self, name) :
        for i in client_list :
            if i.node.name == name :
                self.request.send("Chat_Message|<Server>|That name's already taken :/\r\n")
                return
        if len(name) > len("helloworld") :
            self.request.send("Chat_Message|<Server>|That name's too long :/\r\n")
            return
        if name == "Server" :
            self.request.send("Chat_Message|<Server>|That name's already taken :/\r\n")
            return
        self.node.name = name
        self.request.send("Chat_Message|<Server>|Your name has been changed!\r\n")
        
    # Queue a packet for later sending
    def q_packet(self, packet) :
        self.packet_q.append(packet)

    # Send all the queued packets
    def send_packet_q(self) :
        for i in self.packet_q :
            self.request.send(i)
        self.request.send("Done_Sending_Packets\r\n")
        self.packet_q = []

    # Sends a list of the current main_world's chat messages
    def send_chat_msgs(self) :
        print("Sending chat msgs...")
        for i in range(len(main_world.chat_msgs[-20:])) :
            print("     Sending msg:" + main_world.chat_msgs[i])
            self.q_packet("Chat_Message|"+main_world.chat_msgs[i] + "\r\n")

    def send_chat_msg(self,  name, msg, auth=False) :
        if auth : return
        self.q_packet("Chat_Message|"+name+"|"+msg+"\r\n")

    def set_chat_msg(self, msg) :
        main_world.add_chat_msg(self.node.name, msg)
        f = main_world.format_msg(self.node.name, msg)
        for i in client_list :
            i.send_chat_msg( f[0], f[1])

    # Sets the players position...
    def set_player_position(self, x, y) :
        self.node.x = x
        self.node.y = y

    # Sends over a texture file to the client
    def send_texture_binary(self, texture_id, filetype) :
        opn = open(r.texture_dict[texture_id], "rb")
        img_bin = opn.read() + "<END_OF_PICTURE>\r\n"  # now, it is _possible_ to find that string in the img binary...
        self.request.send("TEXTURE_BIN|"+str(texture_id)+"|"+filetype+"|")
        self.request.sendall(img_bin)
        opn.close()
        del img_bin  # Saves some major memory dude!

    def send_all_texture_binaries(self) :
        count = 0
        print("Sending texture binaries...")
        for i in r.texture_dict :
            count += 1
            print ("    Sending texture no."+str(count)+"...")
            self.send_texture_binary(i, r.texture_dict[i][-3:])
            if not count == len(r.texture_dict) :
                self.request.send("Not_Done_Sending_Textures\r\n")
            print("     Done!")
        self.request.send("Done_Sending_Textures\r\n")

    def send_all_node_data(self) :
        main_world = w.worlds[self.current_world] # If you think this is sloppy, congratulations: you get a fucking cookie
        for i in range(len(main_world.nodes)) :
            if not main_world.nodes[i].node_id == self.node.node_id :
                self.q_packet( main_world.nodes[i].as_packet() )

    def send_dynamic_node_data(self) :
        main_world = w.worlds[self.current_world] # If you think this is sloppy, congratulations: you get a fucking cookie
        for i in range(len(main_world.nodes)) :
            if main_world.nodes[i].node_id == self.node.node_id : continue
            if isinstance(main_world.nodes[i], nt.decorative_node) : continue
            if isinstance(main_world.nodes[i], nt.hitbox_node) : continue
            self.q_packet( main_world.nodes[i].as_packet() )

# Starts the server
print "Running on port: ", port
server = SocketServer.ThreadingTCPServer( ('127.0.0.1', port), client_connection)
server.serve_forever()
