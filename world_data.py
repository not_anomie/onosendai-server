import node_types as nt
import world as wrld

# This bit is what allows the rest of the server code to interface with the world file
# Don't fuck it up.
worlds = []
def get_worlds() : return worlds

# Defines the main world, that is to say the one the player will log into by default
worlds.append(wrld.server_world())
worlds[0].add_node(nt.decorative_node(0,0,z=20000, texture_id=12))
worlds[0].add_node(nt.decorative_node(0,0,z=20000, texture_id=14))
worlds[0].add_node(nt.decorative_node(141,122,z=0, texture_id=15))
worlds[0].add_node(nt.decorative_node(0,0,z=-10000, texture_id=11))
worlds[0].add_node(nt.hitbox_node(-290,27,88,560))
worlds[0].add_node(nt.hitbox_node(8,-260,563,55))
worlds[0].add_node(nt.hitbox_node(278,-23,72,557))
worlds[0].add_node(nt.hitbox_node(140,72,92,35))
worlds[0].add_node(nt.hitbox_node(-2,176,601,179))
worlds[0].add_node(nt.hitbox_node(-46,120,594,67))
worlds[0].add_node(nt.description_node(-143,-53,z=0, texture_id=16, desc="An old fashion CRT TV, they're the best kind!"))
worlds[0].add_node(nt.hitbox_node(-142,-80,86,74))

# Defines a secondary "sub world" that has to be access via a world-changing node (Like doors or portals)
worlds.append(wrld.server_world())
worlds[1].add_node(nt.decorative_node(0,0, z=0, texture_id=6))



