class server_world :
    def __init__(self, world_title="My_World", motd="Welcome to an OpenCPO server- with an admin too lazy to change the MOTD") :
        self.world_title = world_title
        self.nodes = []
        self.chat_msgs = ["<System> | Chat Log Started:", "<System> | " + motd]
        self.seperated_chat_msgs = []

    def get_world_info (self) :
        info = "Set_World_Info|{title}\r\n"
        info = info.format(title=self.world_title)
        return info

    def add_node(self, node) :
        self.nodes.append(node)

    def add_chat_msg(self, name, msg, auth=False) :
        name_msg = self.format_msg(name, msg)
        nme = name_msg[0]
        msg = name_msg[1]
        self.chat_msgs.append(nme+"|"+msg)
        self.seperated_chat_msgs.append([nme,msg])
    
    def format_msg (self, name, msg, auth=False) :
        msg = msg.replace("Done_Sending_Chat_Logs", 'END_MESSAGES')
        msg = msg.replace("|", "-")
        msg = msg.replace("\r", '')
        if auth : nme = "<" + name + "> | "
        else : nme = "<" + name + "[U]>"
        return (nme, msg)
